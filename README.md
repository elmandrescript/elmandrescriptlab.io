# README

This repository serves several purposes as:

* a clean, minimal boilerplate for future front-end projects so that I can stop
either starting from scratch or starting with a full-featured project I have
to strip out. 
* a demo of the basic usage and funcitonality of 
[res-elm](https://www.npmjs.com/package/res-elm), a ReScript
package I wrote for binding to elm ports. 
* ...and more! watch this space. 


