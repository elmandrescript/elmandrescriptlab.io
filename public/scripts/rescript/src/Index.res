open Elm;

/* setup: simple JS dom interop */
@val @scope("document")
external getElementById: string => Dom.element = "getElementById"

@get external getValue: Dom.element => string = "value"
@set external setValue: (Dom.element, string) => unit = "value"

@set
external setOnInput: (Dom.element, Dom.event => unit) => unit = "oninput"

@get
external getTarget: Dom.event => Dom.element = "target"

/* get input element */
let inputReScript: Dom.element = getElementById("input-rescript")

/* ELM INTEROP */

/* declare ports */

module Ports = {
  type t = {
    toElm: Elm.sendable<string>,
    toReScript: Elm.subscribable<string>,
  }
};

/* get app */

let app: Elm.app<Ports.t> =
  Elm.Main.initWithOptions({ node: Some(getElementById("elm-target")),
                             flags: None
                           });

/* wire up ports: ReScript-y way */

inputReScript -> setOnInput(event =>
                          app.ports.toElm
                          -> Elm.send(event->getTarget->getValue)
);

app.ports.toReScript->Elm.subscribe(str => setValue(inputReScript, str))
