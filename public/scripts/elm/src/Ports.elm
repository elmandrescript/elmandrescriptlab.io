port module Ports exposing (..)

import Json.Decode exposing ( Value )

port toReScript : String -> Cmd msg

port toElm : (String -> msg) -> Sub msg
